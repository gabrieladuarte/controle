import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Button } from 'selenium-webdriver';
import { ContaService } from '../service/conta-service';


@Component({
  selector: 'pagar',
  templateUrl: './pagar.page.html',
  styleUrls: ['./pagar.page.scss'],
})
export class PagarPage implements OnInit {
  listaContas;
 
  constructor(
    private conta: ContaService,
    private alert: AlertController,
    private router: Router

  ) { }

  ngOnInit() {
    const url = this.router.url;
    const tipo = url.split('/')[2];
    this.conta.lista(tipo).subscribe(x => this.listaContas = x);
  }

   async remove(conta){
      const confirm = await this.alert.create({
       header: 'Remover Conta',
       message: 'Deseja, realmente, apagar essa conta?',
       buttons: [{
         text: 'Cancelar',
         role: 'cancel'
       },{
            text: 'Deletar',
            handler: () => this.conta.remove(conta)
       }
      ]
      });
      confirm.present();
    }

   async edita(conta){
      const confirm = await this.alert.create({
        header: 'Editar Conta',
        inputs:[
          {
            name:'parceiro',
            value: conta.parceiro,
            placeholder: 'Parceiro Comercial'
          }, {
                 name: 'descricao',
                 value: conta.descricao,
                 placeholder: 'Descrição'
          }, {
            name: 'valor',
            value: conta.valor,
           type: 'number'
          }],
        buttons: [{
          text: 'Cancelar',
          role: 'cancel'
        },{
             text: 'Enviar',
             handler: (data) =>{ 
              conta.descricao = data.descricao;
              conta.parceiro = data.parceiro;
              conta.valor = data.valor;
               this.conta.edita(conta)
             }
        }
       ]
       });
       confirm.present();
    }

}
