import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { environment } from 'src/environments/environment.prod';

import { AngularFireAuthModule} from '@angular/fire/compat/auth';
import { AngularFireModule} from '@angular/fire/compat';
import {AngularFireStorageModule } from '@angular/fire/compat/storage';
import {AngularFirestoreModule} from '@angular/fire/compat/firestore';
import { registerLocaleData } from '@angular/common';
import localePT from '@angular/common/locales/pt'
registerLocaleData(localePT);

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), 
    AngularFireAuthModule, AngularFireStorageModule, 
    AngularFirestoreModule.enablePersistence(), AngularFireModule.initializeApp(environment.firebaseConfig),
    AppRoutingModule],
  providers: [
    {provide: LOCALE_ID, useValue: 'pt-BR'},
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
