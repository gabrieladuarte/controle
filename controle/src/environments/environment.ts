// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDcju1_mPWe_iiNwJX3mD1aR2PnZ7aqZp0',
    authDomain: 'controle-if3.firebaseapp.com',
    projectId: 'controle-if3',
    storageBucket: 'controle-if3.appspot.com',
    messagingSenderId: '170234277086',
    appId: '1:170234277086:web:3df6df86be34aa87c9264f',
    measurementId: 'G-5LWRL4P5J9'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
